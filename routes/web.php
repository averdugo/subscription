<?php

Route::get('/', function () {
    return redirect('login');
});

//Auth::routes(['verify' => true]);
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/convenios', 'HomeController@convenios');

Route::get('/videos', 'HomeController@videos');

Route::get('/usuarios', 'HomeController@usuarios');

Route::get('/ventas', 'HomeController@ventas');

Route::get('/suscrpciones', 'HomeController@subscription');

Route::get('/subscriptionselect', 'SubscriptionController@subscriptionselect');

Route::resource('/posts','PostController');

Route::resource('/sales','SaleController');

Route::resource('/vistas','ViewsController');

Route::resource('/users','UserController');

Route::resource('/subscription','SubscriptionController');

Route::get('/userlogout', 'HomeController@logout');

Route::get('/goFlow', 'SaleController@goFlow');

Route::get('/perfil', 'HomeController@profile');

Route::get('/changeSub/{id}', 'SubscriptionController@changeSub');

Route::get('/getVideosMonth/{month}', 'PostController@getVideosMonth');

Route::post('/flowConfirm', 'SaleController@flowConfirm');

Route::post('/flowResult', 'SaleController@flowResult');


Route::get('user_list_excel', 'UserController@exportExcel')->name('users.excel');

Route::get('/user/sendmail/{id}', 'SendMailController@userShow');

Route::post('/user/mailsend/{id}', 'SendMailController@mailSend');

Route::post('/usersChangePassword/{id}', 'UserController@passchange');

Route::get('/user/newVideoMail', 'SendMailController@videoMail');

Route::get('/sendMailInactive' , 'SendMailController@sendMailInactive');

Route::get('/emails' , 'SendMailController@index');

Route::get('/emails/{id}' , 'SendMailController@edit');

Route::put('/emails/{id}' , 'SendMailController@update');