<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('emails')->insert([
            'subject' => "Aviso de Nuevo Video",
            'body' => "",
            'type' => 1
        ]);
        DB::table('emails')->insert([
            'subject' => "Cuenta Pronto a Desactivarse",
            'body' => "",
            'type' => 2
        ]);
    }
}
