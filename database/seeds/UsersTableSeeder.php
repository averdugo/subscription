<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*DB::table('users')->insert([
            'f_name' => "Aldo",
            'l_name' => "Verdugo",
            'rut' => "16250774-5",
            'birthday_at' => "1985-12-03",
            'gender' => 1,
            'region_id' => 1,
            'provincia_id' => 1,
            'subscription_id' => 1,
            'type' => 1,
            'status' => 2,
            'email' => 'admin@gmail.com',
            'password' => Hash::make('secret'),
        ]);
        /*DB::table('users')->insert([
            'f_name' => "Admin",
            'l_name' => "Sindical",
            'rut' => "12456789-5",
            'birthday_at' => "1995-12-03",
            'gender' => 1,
            'region_id' => 1,
            'provincia_id' => 1,
            'subscription_id' => 1,
            'type' => 1,
            'status' => 2,
            'email' => 'admin@gmail.com',
            'password' => Hash::make('sindical123'),
        ]);*/

    }
}
