<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubscriptionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subscriptions')->insert([
            'name' => "Mensual",
            'description' => "Acceso a 4 videos",
            'price' => "$ 3.990"
        ]);
        DB::table('subscriptions')->insert([
            'name' => "Semestral",
            'description' => "Acceso a 26 videos",
            'price' => "$ 19.990"
        ]);
        DB::table('subscriptions')->insert([
            'name' => "Anual",
            'description' => "Acceso a 52 videos",
            'price' => "$ 34.990"
        ]);
    }
}
