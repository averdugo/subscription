<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Views extends Model
{
    protected $fillable = [
        'user_id', 'subscription_id', 'post_id'
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function subscription()
    {
        return $this->belongsTo('App\Subscription');
    }
    public function post()
    {
        return $this->belongsTo('App\Post');
    }

}
