<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{User, Email};
use Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\{MessageRecived, AdminInacctiveAccount, uploadedVideo, cuentaDesactiva, passRecovery};

class SendMailController extends Controller
{
    public function index(){
        $emails = Email::all();
        return view('emailcrud.list', ['data'=>$emails, 'menu'=>9]);
    }

    public function edit($id){
        $email = Email::find($id);
        return view('emailcrud.edit', ['email'=>$email, 'menu'=>4]);
    }

    public function update(Request $request, $id){
        $email = Email::find($id);
        $email->fill($request->all());
        $email->update();
        return back()->with('status', 'Actualizado con Exito!');
    }

    public function sendMailInactive(){
        $users = User::where('type', 2)->where('status','<>',4)->get();
        $emailData = Email::where('type',2)->first();
        $inactiveAccounts = [];
        foreach ($users as $key => $u) {
            if($u->missingdays == 5){
                array_push($inactiveAccounts, $u);
                Mail::to($u->email)->queue(new cuentaDesactiva($u, $emailData));
            }
        }
        Mail::to('servicio@mundosindicalapp.cl')->queue(new AdminInacctiveAccount($inactiveAccounts));
    }
    
    public function userShow($id){
        $user = User::find($id);
        return view('usuarios.sendEmail' , ['user'=>$user, 'menu'=>6]);
        
    }

    public function mailSend(Request $request, $id){
        $user = User::find($id);
        $mensaje = ([
            $user->email,
            $request->asunto,
            $request->mensaje
            
        ]) ;

        Mail::to($user->email)->send(new MessageRecived($mensaje));
        return redirect('/usuarios')->with('status', 'Correo Enviado');
    }
    public function videoMail(){
        $user = User::where('type', 3)->first();
        Mail::to($user->email)->queue(new uploadedVideo($user));
    }
    public function cuentaDesactiva(){
        Mail::to('asessin520@gmail.com')->send(new cuentaDesactiva());
    }
    

}
