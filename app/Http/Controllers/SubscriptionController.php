<?php

namespace App\Http\Controllers;

use App\Subscription;
use Illuminate\Http\Request;
use Auth;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $subs = Subscription::create([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
        ]);

        return back()->with('status', 'Suscripcion Creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Subscription::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subs = Subscription::find($id);
        $subs->fill($request->all());

        $subs->update();
        return back()->with('status', 'Actualizado con Exito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Subscription::find($id)->delete();

        return back()->with('status', 'Suscripcion Eliminada');
    }

    public function changeSub($id)
    {
        $user = Auth::user();
        $user->subscription_id = $id;
        if($user->update()){
            return 1;
        }

    }

    public function subscriptionselect()
    {
        $subs = Subscription::all();
        $data = "";
        foreach ($subs as $v) {
            $data .= sprintf(
                '<option value="%s">%s %s / %s</option>',
                $v->id,
                $v->name,
                $v->description,
                $v->price
            );
        }
        return $data;
    }
}
