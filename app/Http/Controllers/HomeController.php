<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Post, Views, User, Sale, Subscription};
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\cuentaDesactiva;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //$now = Carbon::now();
        $user = Auth::user();
        $sale = Sale::where('user_id', $user->id)
                    ->orderBy('created_at', 'DESC')
                    ->first();
        if($user->type == 1){

            $posts = Post::where('status', 1)->where('type',1)->get();
            return view('home', ['user'=>$user, 'posts'=>$posts, 'menu'=>1, 'payMonths' =>12]);
        }
        if($user->type == 3){

            $posts = Post::where('status', 1)->where('type',1)->get();
            return view('home', ['user'=>$user, 'posts'=>$posts, 'menu'=>1, 'payMonths' =>12]);
        }
        if(!$sale || $sale->status == 9 || User::getSubscriptionStatus($sale , $user->subscription_id)){
            $user->status = 4;
            $user->update(); 
            return view('payments', ['user'=>$user]);
        }else{
            $payMonths = 0;
            if($user->subscription_id != 3) {
                foreach ($user->sales as $key => $s) {
                    if($s->status == 1) {
                        $payMonths++;
                    }
                }
                $payMonths = $user->subscription_id == 2 ? $payMonths * 6 : $payMonths;
            }else{
                $payMonths = 12;
            }
            $posts = Post::where('status', 1)->where('type',2)->get();
            return view('home', ['user'=>$user, 'posts'=>$posts, 'menu'=>1, 'payMonths' => $payMonths]);
        }
        
       
    }
    public function convenios()
    {
        $user = Auth::user();

        if($user->type == 1){
            $posts = Post::where('type', 1)->get();
            return view('convenios.list', ['data'=>$posts, 'menu'=>2]);
        }else{
            $posts = Post::where('type', 1)->where('status', 1)->get();
            return view('convenio', ['user'=>$user, 'posts'=>$posts, 'menu'=>2]);
        }
        
    }
    public function videos()
    {
        $posts = Post::where('type', 2)->get();
        return view('videos.list', ['data'=>$posts, 'menu'=>4]);
    }
    public function ventas()
    {
    
        $sales = Sale::all();
        $status = Sale::STATUS;
        return view('ventas.list', ['data'=>$sales, 'status'=>$status,'menu'=>5]);
    }
    public function usuarios()
    {
        $sus = Subscription::all();
        $users = User::where('type', 2)
                ->orwhere('type' , 3)
                ->get();
        return view('usuarios.list', ['data'=>$users, 'menu'=>6, 'sus'=>$sus]);
        
    }
    public function profile()
    {
        $sus = Subscription::all();
        $user = Auth::user();
        return view('profile', ['user'=>$user, 'menu'=>3, 'sus'=>$sus]);
    }
    public function subscription()
    {
        $sus = Subscription::all();
        $user = Auth::user();
        return view('suscripciones.list', ['data'=>$sus, 'user'=>$user, 'menu'=>7, 'sus'=>$sus]);
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
