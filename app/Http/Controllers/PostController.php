<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use App\{User, Email};
use App\Mail\uploadedVideo;
use Illuminate\Support\Facades\Mail;



class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $post = Post::create($request->all());
        if ($request->file('fileImg')) {
            $path = $request->file('fileImg')->store('public');
            $img = str_replace('public/', '', $path);
            $post->img_path = $img;
            $post->save();
        }
        
        if($request->video_path !== ''){
            $users = User::where('type', 2)->get();
            $emailData = Email::where('type', 1)->first();
            foreach ($users as $key => $user) {
                Mail::to($user->email)->queue(new uploadedVideo($user, $emailData));
            }
            return redirect('/videos')->with('status', 'Video subido');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return $post;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
       
        $post->fill($request->all());
        if ($request->file('fileImg')) {
            $path = $request->file('fileImg')->store('public');
            $img = str_replace('public/', '', $path);
            $post->img_path = $img;
            
        }
        $post->update();
        return back()->with('status', 'Actualizado con Exito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return back()->with('status', 'Eliminado con Exito!');
    }

    public function getVideosMonth($month){
        $videos = Post::where('type', 2)->where('month', $month)->get();
        $data = '';
        foreach ($videos as $key => $i) {
            $data .= sprintf('<div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-xl-9 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="user-avatar float-xl-left pr-4 float-none">
                            <img src="/storage/%s" alt="User Avatar" class="user-avatar-xl">    
                        </div>
                        <div class="pl-xl-3">
                            <div class="m-b-0">
                                <div class="user-avatar-name d-inline-block">
                                    <h2 class="font-24 m-b-10">%s</h2>
                                </div>
                            </div>
                            <div class="user-avatar-address">
                                <p class="mb-2">%s</p>
                                        
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="float-xl-right float-none mt-xl-0 mt-4">
                                <a href="#" data-id="%s" class="btn btn-primary postModal">Ver Video</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>  ', $i->img_path, $i->title, $i->description, $i->id  );
        }
        return $data;
    }
}
