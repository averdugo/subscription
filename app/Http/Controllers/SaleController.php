<?php

namespace App\Http\Controllers;

use App\{Sale, User, Subscription};
use Illuminate\Http\Request;
use App\Http\Controllers\FlowApi;
use Auth;

class SaleController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function goFlow()
    {
        
        $sub = Subscription::find(Auth::user()->subscription_id);
        $total = intval(str_replace("$","",str_replace(".","",$sub->price)));
        $sale = new Sale;
		$sale->user_id = Auth::user()->id;
		$sale->subscription_id = Auth::user()->subscription_id;
		$sale->amount = $total;
		$sale->status = 9;
        $sale->save();
        
        
        $optional = array(
			"rut" => Auth::user()->rut,
			"id" => $sale->id,
		);

		$optional = json_encode($optional);

		$params = array(
			"commerceOrder" => $sale->id,
			"subject" => "Pago en Mundo Sindical",
			"currency" => "CLP",
			"amount" => $total,
			"email" => Auth::user()->email,
			"paymentMethod" => 9,
			"urlConfirmation" =>  "https://suscripcion.mundosindicalapp.com/flowConfirm",
			"urlReturn" => "https://suscripcion.mundosindicalapp.com/flowResult",
			"optional" => $optional
		);

		$serviceName = "payment/create";
		$flowApi = new FlowApi();
		$response = $flowApi->send($serviceName, $params,"POST");
		$redirect = $response["url"] . "?token=" . $response["token"];

		return redirect($redirect);
    }
    public function flowConfirm(Request $request)
	{
		
		try {
			if(!isset($_POST["token"])) {
				throw new Exception("No se recibio el token", 1);
			}
			$token = filter_input(INPUT_POST, 'token');
			$params = array(
				"token" => $request->token
            );
            
			$serviceName = "payment/getStatus";
			$flowApi = new FlowApi();
			$response = $flowApi->send($serviceName, $params, "GET");
			$sale = Sale::find($response['optional']['id']);
			if ($response['status'] == 2) {
				$sale->status = 1;
				$sale->save();
				return redirect('/home')->with('status', 'Inscripcion Aprobada!');
			}else{
				$sale->status = 2;
				$sale->save();
				return back()->with('status', 'Inscripcion Rechazada!');
			}
		} catch (Exception $e) {
			echo "Error: " . $e->getCode() . " - " . $e->getMessage();
		}
    }
    public function flowResult(Request $request)
	{
        try {
			if(!isset($_POST["token"])) {
				throw new Exception("No se recibio el token", 1);
			}
			$token = filter_input(INPUT_POST, 'token');
			$params = array(
				"token" => $request->token
            );
            
			$serviceName = "payment/getStatus";
			$flowApi = new FlowApi();
			$response = $flowApi->send($serviceName, $params, "GET");
			$sale = Sale::find($response['optional']['id']);
			if ($response['status'] == 2) {
				$sale->status = 1;
				$sale->save();
				return redirect('/home')->with('status', 'Inscripcion Aprobada!');
			}else{
				$sale->status = 2;
				$sale->save();
				return back()->with('status', 'Inscripcion Rechazada!');
			}
		} catch (Exception $e) {
			echo "Error: " . $e->getCode() . " - " . $e->getMessage();
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Sale::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale $sale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sale = Sale::find($id);
        $sale->fill($request->all());
        $sale->update();
        
        $user = $sale->user;
        switch ($sale->status) {
            case 9:
                $user->status = 4;
                $user->update(); 
                break;
            case 1:
                $user->status = 2;
                $user->update(); 
                break;
            case 2:
                $user->status = 3;
                $user->update(); 
                break;
        }
        
        return back()->with('status', 'Actualizado con Exito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Sale::find($id)->delete();

        return back()->with('status', 'Venta Eliminada');
    }
}
