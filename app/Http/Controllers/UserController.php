<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
use App\Sale;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //TODO Haceer Validaciones
		/*$this->validate($request, [
			'name' => 'required',
			'email' => 'required',
			'password' => 'required',
            'type' => 'required',
            //'subscription_id'  => 'required'
		]);
        */

        $user = User::create([
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'rut' => $request->rut,
            'phone' => $request->phone,
            'type' => $request->type,
            'subscription_id' => $request->subscription_id,
            'status' => 2,
            'email'=> $request->email,
            'password' => Hash::make($request->password),
            'age'=> $request->age,
        ]);
        
        if($request->type == 2){
            $sale = new Sale;
            $sale->user_id = $user->id;
            $sale->subscription_id = $user->subscription_id;
            $sale->amount = 1;
            $sale->status = 1;
            $sale->save();
        } 
        return back()->with('status', 'Usuario Creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $user = User::find($id);
        $user->fill($request->all());
        
        
        if ($request->file('fileImg')) {
            $path = $request->file('fileImg')->store('public');
            $img = str_replace('public/', '', $path);
            $user->img = $img;
            
        }
        $user->update();
        return back()->with('status', 'Actualizado con Exito!');
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        Sale::where('user_id', $id)->delete();

        return back()->with('status', 'Usuario Eliminado');
    }

    public function exportExcel(){

        return Excel::download(new UsersExport, 'user-list.xlsx');
    }

    public function passchange(Request $request, $id){
        $user = User::find($id);
        $user->fill($request->all());
        $user->password = Hash::make($request->password);
        $user->update();
        
        return back()->with('status', 'Contraseña cambiada');
    }
}
