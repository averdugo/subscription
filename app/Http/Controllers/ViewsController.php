<?php

namespace App\Http\Controllers;

use App\{Views, User, Sale};
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;

class ViewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $sale = Sale::where('user_id', $user->id)
                    ->where('status', 1)
                    ->orderBy('created_at', 'DESC')
                    ->first();
        
        $views = Views::where('created_at','>',$sale->created_at)
                    ->count();
             
        if($user->subscription_id == 1 && $views >= 4){
            return 0;
        }else if ($user->subscription_id == 2 && $views >= 26){
            return 0;
        }else{
            Views::create([
                'user_id' => $user->id,
                'subscription_id' => $user->subscription_id,
                'post_id' => $request->postId
            ]);
    
            return 1;
        }
        
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Views  $views
     * @return \Illuminate\Http\Response
     */
    public function show(Views $views)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Views  $views
     * @return \Illuminate\Http\Response
     */
    public function edit(Views $views)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Views  $views
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Views $views)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Views  $views
     * @return \Illuminate\Http\Response
     */
    public function destroy(Views $views)
    {
        //
    }
}
