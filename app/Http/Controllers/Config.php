<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\ModelNotFoundException;



 class Config {

	static function get($name) {
		
		$COMMERCE_CONFIG = array(
			"APIKEY" => "47D8FAED-8EE5-4E6E-A59A-341LB7C27056", // Registre aquí su apiKey
			"SECRETKEY" => "edc84bc2c4f088308529b401305d1588f37b8a45", // Registre aquí su secretKey
			"APIURL" => "https://www.flow.cl/api", // Producción EndPoint o Sandbox EndPoint
			"BASEURL" => "http://suscripcion.mundosindicalapp.com/apiFlow" //Registre aquí la URL base en su página donde instalará el cliente
		 );

		if(!isset($COMMERCE_CONFIG[$name])) {
			throw new ModelNotFoundException("The configuration element thas not exist", 1);
		}
		return $COMMERCE_CONFIG[$name];
	}
 }
