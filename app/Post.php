<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title', 'description', 'video_path', 'img_path', 'type', 'status', 'month'
    ];

    const TYPE = [
        1=>'Convenio',
        2=>'Video',
        3=>'Otro',
    ];

    const STATUS = [
        0 => 'Inactivo',
        1 => 'Activo',
    ];

    public function views()
    {
        return $this->hasMany('App\Views');
    }

    public function getStatusLabelAttribute()
    {
        return Self::STATUS[$this->status];
    }

}
