<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Sale extends Model
{
    protected $fillable = [
        'user_id', 'subscription_id', 'amount', 'status'
    ];

    const STATUS = [
        9=>"Pendiente",
        1=>'Aprobada',
        2=>'Anulada' 
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function subscription()
    {
        return $this->belongsTo('App\Subscription');
    }
    
    public function getStatusLabelAttribute()
    {
        return Self::STATUS[$this->status];
    }
    public function getDaysAttribute()
    {
        $now = Carbon::now();
        $dateofSale = Carbon::parse($this->created_at);
        $ddDays = $now->diffInDays($dateofSale);
        
        if($this->status != 1)
            return 0;
        

        switch ($this->subscription_id) {
            case 1:
                return $ddDays > 30 ? 0 : 30 - $ddDays;
                break;
            case 2:
                return $ddDays > 180 ? 0 : 180 - $ddDays;
                break;
            case 3:
                return 365 - $ddDays;
                break;
        }
    }
}
