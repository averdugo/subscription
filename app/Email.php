<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{

    protected $fillable = ['subject', 'body', 'type'];

    const TYPE = [
        1=>'Video Nuevo',
        2=>'Suscripcion Vencida'
    ];
    

    public function getTypeLabelAttribute()
    {
        return Self::TYPE[$this->type];
    }
}
