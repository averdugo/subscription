<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;
use App\Sale;
use App\Notifications\UserResetPassword;

class User extends Authenticatable 
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'f_name', 'l_name', 'rut', 'phone', 'address', 'gender', 'region_id', 'provincia_id', 'subscription_id', 'type', 'status',  'email', 'password','last_login_at', 'last_login_ip', 'city', 'imagen'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    const TYPES = array(
        1 => 'Administrador',
        2 => 'Usuario'
    );
    const STATUS = array(
        1 => 'Pendiente',
        2 => 'Activo',
        3 => 'Cancelado',
        4 => 'Renovacion'
    );

    public function region()
    {
        return $this->hasOne('App\Region');
    }
    
    public function provincia()
    {
        return $this->hasOne('App\Provincia');
    }

    public function subscription()
    {
        return $this->belongsTo('App\Subscription');
    }

    public function sales()
    {
        return $this->hasMany('App\Sale');
    }
    
    public function views()
    {
        return $this->hasMany('App\Views');
    }

    public function getFullNameAttribute()
    {
        return "{$this->f_name} {$this->l_name}";
    }

    public function getAgeAttribute()
    {

        return Carbon::parse($this->birthday_at)->age;
    }
    
    public function getMonthviewsAttribute()
    {
        $now = Carbon::now();
        $views = $this->views;
        $count = 0; 
        
        foreach ($views as $k => $v) {
            $vDate = Carbon::parse($v->created_at);
            if($vDate->month == $now->month)
                $count++;
        }
        
        return $count;
    }
    public function getYearviewsAttribute()
    {
        $now = Carbon::now();
        $views = $this->views;
        $count = 0; 
        
        foreach ($views as $k => $v) {
            $vDate = Carbon::parse($v->created_at);
            if($vDate->year == $now->year)
                $count++;
        }
        
        return $count;
    }

    public function getMissingvideosAttribute()
    {
        switch ($this->subscription_id) {
            case 1:
                return 4 - $this->monthviews;
                break;
            case 2:
                return 26 - $this->yearviews;
                break;
            case 3:
                return 52 - $this->yearviews;
                break;
        }
    }
    
    public function getMissingdaysAttribute()
    {
        if($this->type !== 2) return 365;

        $sale = Sale::where('user_id', $this->id)
                    ->where('status', '<>',9)
                    ->orderBy('created_at', 'DESC')
                    ->first();
        if(!$sale) return 0;
        $now = Carbon::now();
        $dateofSale = Carbon::parse($sale->created_at);
        $ddDays = $now->diffInDays($dateofSale);   


        switch ($this->subscription_id) {
            case 1:
                return $ddDays > 30 ? 0 : 30 - $ddDays;
                break;
            case 2:
                return $ddDays > 180 ? 0 : 180 - $ddDays;
                break;
            case 3:
                return 365 - $ddDays;
                break;
        }
    }
    
    public static function getSubscriptionStatus($sale, $sId){
        $now = Carbon::now();
        $dateofSale = Carbon::parse($sale->created_at);
        $ddDays = $now->diffInDays($dateofSale);   
        
        switch ($sId) {
            case 1:
                return $ddDays > 30 ? true : false;
                break;
            case 2:
                return $ddDays > 180 ? true : false;
                break;
            case 3:
                return false;
                break;
        }
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new UserResetPassword($token));
    }

}
