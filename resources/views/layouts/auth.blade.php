<!doctype html>
<html lang="en">
 
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Mundo Sindical</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.min.css">
        <link href="/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
        <link rel="stylesheet" href="/assets/libs/css/style.css">
        <link rel="stylesheet" href="/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="/assets/vendor/datepicker/tempusdominus-bootstrap-4.css" />
        <style>
        html,
        body {
            height: 100%;
        }

        body {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-align: center;
            align-items: center;
            padding-top: 40px;
            padding-bottom: 40px;
        }
        </style>
    </head>

    <body>
        @yield('content')
   
        <script src="/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
        <script src="/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
        <script src="/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
        <script src="/assets/libs/js/main-js.js"></script>
        <script src="/assets/vendor/datepicker/moment.js"></script>
        <script src="/assets/vendor/datepicker/tempusdominus-bootstrap-4.js"></script>
        <script src="/assets/vendor/datepicker/datepicker.js"></script>
        @yield('scripts')
    </body>
    
</html>