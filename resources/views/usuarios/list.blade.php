@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/buttons.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/select.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/fixedHeader.bootstrap4.css">
@endsection

@section('content')
    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

    </div>
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header row">
                <div class="col-md-5 align-self-center">
                    <h2 class="mb-2">Usuarios </h2>
                </div>
                <div class="row">
                    <div class="col-md-7 row ">
                        <a href="#" data-toggle="modal" data-target="#createModalC" class="btn waves-effect waves-light btn  pull-right hidden-sm-down" style="
                        background-color: #8d68a9;
                        border-color: #8d68a9; color:white">Crear Usuario</a>
                    </div>
                    @include('usuarios.crearUser')
                    <div class="row ml-1"><a href="{{ route('users.excel')}} " class="btn waves-effect waves-light btn  pull-right hidden-sm-down" style="
                        background-color: #8d68a9;
                        border-color: #8d68a9; color:white">Exportar Usuarios</a></div>
                </div>
            </div>

        </div>
    <div class="row" style="width:100%">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered first">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Rut</th>
                                    <th>Edad</th>
                                    <th>Email</th>
                                    <th>Telefono</th>
                                    <th>tipo de usuario</th>
                                    <th>Suscripcion</th>
                                    <th>Consumo Videos</th>
                                    <th>Ultimo Login</th>
                                    <th style="width: 10%;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $i)
                                    <tr>
                                        
                                        <td>{{$i->full_name}}</td>
                                        <td>{{$i->rut}}</td>
                                        <td>{{$i->age}}</td>
                                        <td>{{$i->email}}</td>
                                        <td>{{$i->phone}}</td>
                                        <td>{{$i->type}}</td>
                                        
                                        
                                        <td>{{$i->subscription ? $i->subscription->name:''}}</td>

                                        <td>{{count($i->views)}}</td>
                                        <td>{{ date('d-M-Y', strtotime($i->last_login_at)) }}</td>
                                        <td class="row">
                                            
                                            <a href="#" class="btn btn-default goEdit row" data-id="{{$i->id}}" style="width: 65px; padding: 12px;">
                                                <i data-toggle="tooltip" data-placement="top" title="Editar" class="fa fa-pencil-alt" style="font-size: 18px;"></i>
                                            </a>

                                            <a href="/user/sendmail/{{$i->id}} " class="btn btn-default row" style="width: 65px; padding:12px;">
                                                <i data-toggle="tooltip" data-placement="top" title="mail" class="fas fa-envelope" style="font-size: 18px;"></i>
                                            </a>

                                            <a href="#" class="btn btn-default row passEdit"  data-id="{{$i->id}}" style="font-size: 18px;width: 65px; padding: 10;">
                                                <i data-toggle="tooltip" data-placement="top" title="cambio de contraseña" class="fab fa-expeditedssl" ></i>
                                            </a>

                                            

                                            <form action="/users/{{$i->id}}" method="post" class="row">
                                                @csrf
                                                {{ method_field('delete') }}
                                                <button class="btn ml-4px" type="submit" style="width: 40px; padding: 0; color:gray; margin-left: 20px;">
                                                    <i data-toggle="tooltip" data-placement="top" title="Borrar" class="fa fa-trash-alt "></i>
                                                </button>
                                            </form>
                                                
                                            
                                        </td>
                                        
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Rut</th>
                                    <th>Edad</th>
                                    <th>Email</th>
                                    <th>Telefono</th>
                                    <th>tipo de usuario</th>
                                    <th>Suscripcion</th>
                                    <th>Consumo Videos</th>
                                    <th>Ultimo Login</th>
                                    <th style="width: 10%;"></th>
                                    
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <img src="{{public_path('assets/images/header.png')}}" alt="">
    </div>
    @include('usuarios.edit')
    @include('usuarios.pass')
@endsection

@section('scripts')
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="/assets/vendor/datatables/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="/assets/vendor/datatables/js/buttons.bootstrap4.min.js"></script>
    <script src="/assets/vendor/datatables/js/data-table.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/17.0.0/classic/ckeditor.js"></script>
    <script src="/assets/vendor/datepicker/moment.js"></script>
        <script src="/assets/vendor/datepicker/tempusdominus-bootstrap-4.js"></script>
        <script src="/assets/vendor/datepicker/datepicker.js"></script>

    <script>
        

        $(function(){


            $("body").on('click','.goEdit', function(){
                var userId = $(this).data('id');
                $.get('/users/' + userId, function( response ){
                    var user = response;
                    
                    $('#editModal form').attr('action', "/users/"+user.id)
                    
                    $('#editModal input[name=f_name]').val(user.f_name)
                    $('#editModal input[name=l_name]').val(user.l_name)
                    $('#editModal input[name=email]').val(user.email)
                    $('#editModal input[name=password]').val(user.password)
                    $('#editModal input[name=rut]').val(user.rut)
                    $('#editModal input[name=phone]').val(user.phone)
                    $('#editModal select[name=subscription_id]').val(user.subscription_id)

                    $('#editModal').modal()

                })
                
            })

            $("body").on('click', '.cancelEdit', function(e){
                e.preventDefault()
                location.reload()
            });
            
            $("body").on('click','.passEdit', function(){
                
                var userId = $(this).data('id');
                console.log(userId)
                
                
                $('#passModal form').attr('action', "/usersChangePassword/"+userId)
                $('#passModal').modal()
            })

            $("body").on('click', '.cancelEdit', function(e){
                e.preventDefault()
                location.reload()
            })
        })
    </script>
@endsection
