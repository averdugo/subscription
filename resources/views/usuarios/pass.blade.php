<div class="modal fade" id="passModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Editar Contraseña </h5>
		  			<button type="button" class="close  cancelEdit" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
		 			 </button>
        </div>
        <form action="/usersChangePassword/+userId" method="POST">
          @csrf
          <div class="modal-body">
                <label for="">Password</label>
                <input class="form-control form-control-lg" type="password" name="password" required="">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button type="submit" value="" style="background-color: #8d68a9;border-color: #8d68a9;color:white; width: 117px;height: 46px;" placeholder="Cambiar">Cambiar</button>
          </div>
        </form>     
      </div>
    </div>
  </div>