<div class="modal fade" id="createModalC" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
	  	<div class="modal-content">
			<div class="modal-header">
		  		<h5 class="modal-title" id="exampleModalLabel">Crear Usuario</h5>
		  			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
		 			 </button>
			</div>
			<form action="/users" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="modal-body">

					<div class="form-group row">
						<div class="form-group col-md-6">
							<label for="">Nombre</label>
							<input class="form-control form-control-lg" type="text" name="f_name" required=""  >
						</div>
						<div class="form-group col-md-6">
							<label for="">Apellido</label>
							<input class="form-control form-control-lg" type="text" name="l_name" required="" >
						</div>
					</div>
					<div class="form-group row">
						<div class="form-group col-md-6">
							<label for="">E-mail</label>
							<input class="form-control form-control-lg @error('email') is-invalid @enderror" type="email" name="email" required="" autocomplete="off" value="{{ old('email') }}">
							@error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="form-group col-md-6">
							<label for="">Password</label>
							<input class="form-control form-control-lg" type="password" name="password" required="" >
						</div>
					</div>
					<div class="form-group row">
						<div class="form-group col-md-6">
							<label for="">Rut</label>
							<input class="form-control form-control-lg" type="text" name="rut" required=""  autocomplete="off" placeholder="12345465-9">
						</div>
						<div class="form-group col-md-6">
							<label for="">Telefono</label>
							<input class="form-control form-control-lg" type="text" name="phone" required="" placeholder="+569-12345678">
						</div>
					</div>
					<div class="form-group row">
						<div class="form-group col-md-6">
							<label for="">Suscripcion</label>
							<select class="form-control form-control-lg" name="subscription_id" >
								<option value="">Seleccione</option>
								@foreach ($sus as $s)
									<option value="{{$s->id}}">{{$s->name}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group col-md-6">
							<label for="">Edad</label>
							<input class="form-control form-control-lg" type="text" name="age" required=""  >
						</div>
					</div>
					<div class="form-group row">
						<div class="form-group col-md-6">
							<label for="">Tipo</label>
							<select class="form-control form-control-lg" name="type">
								<option value="">Tipo de usuario</option>
								<option value="3">Admin</option>
								<option value="2">User</option>
							</select>
						</div>
					</div>
					<div class="modal-footer">
						<div class="form-group pt-2 col-md-12 text-right">
							<button class="btn  btn-success" type="submit" style="
							background-color: #8d68a9;
							border-color: #8d68a9; color:white">Guardar</button>
						</div>
					</div>
				</div>
			</form>										
	  	</div>
	</div>
</div>