@extends('layouts.app')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .cke_inner {
            width: 100%;
        }
        .ck.ck-editor {
            width: 50%;
        }
    </style>
@endsection

@section('content')
<div>
    <h3 style="margin: 20px; margin-left: 43px;">Enviar Correo al Usuario {{$user->f_name}} </h3>
    <div class="row" style="width:100%">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-body">
                    <div class="">
                        <form method="POST" action="/user/mailsend/{{$user->id}}" >
                            @csrf
                            <div class="form-group ">
                                <div class="form-group col-md-12 row">
                                    <label for="input" class="col-md-5 row ml-4" style="height: 100%;font-size: 30px;">Asunto</label>
                                    <input class="form-control form-control-lg col-md-6 row" type="text" placeholder="" style="height: 43px; left: -9px;" name="asunto">
                                </div>
                                <div class="form-group col-md-12 row">
                                    <label for="input" class="col-md-5 ml-4" style="height: 100%;font-size: 25px;margin-right: -40px !important;">Mensaje para el usuario</label>
                                    
                                    <textarea  class="form-control form-control-lg col-md-6 row" style="height: 80px;margin-left: -24px;max-width: 100%;"  name="mensaje" id="editor"></textarea>
                                </div>
                                <button type="submit" class="btn btn-outline-success btn-block btn-lg btnGoFlow" style="width: 200px; position:relative; top: 50%; left:607px; margin-top: 15px;">Enviar mail</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
    <script src="https://cdn.ckeditor.com/ckeditor5/17.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>
@endsection