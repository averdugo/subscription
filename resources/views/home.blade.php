@extends('layouts.app')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h3 class="mb-2">Videos - Mes <span id="monthLabel">{{$payMonths}}</span></h3>
                <p class="pageheader-text">Bienvenidos a Mundo Sindical.</p>
            </div>
        </div>
    </div>
    <div class="row">
       
        <div id="videoRow" class="col-xl-9 col-lg-8 col-md-8 col-sm-12 col-12">
            @foreach ($posts as $i)
                @if ($i->month == $payMonths)
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-xl-9 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="user-avatar float-xl-left pr-4 float-none">
                                    <img src="{{ asset("storage/$i->img_path")}}" alt="User Avatar" class="user-avatar-xl">    
                                </div>
                                <div class="pl-xl-3">
                                    <div class="m-b-0">
                                        <div class="user-avatar-name d-inline-block">
                                            <h2 class="font-24 m-b-10">{{$i->title}}</h2>
                                        </div>
                                    </div>
                                    <div class="user-avatar-address">
                                        <p class="mb-2">{!!$i->description!!}</p>
                                                
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="float-xl-right float-none mt-xl-0 mt-4">
                                    @if ($i->type==1)
                                        <a href="#" data-id="{{$i->id}}" class="btn btn-primary postModal" >Leer Mas</a>    
                                    @else
                                        <a href="#" data-id="{{$i->id}}" class="btn btn-primary postModal">Ver Video</a>
                                    @endif
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
                @endif
                
            @endforeach
            </div>
            <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12">
                <div class="card">
                    <div class="card-body border-top">
                        <h3 class="font-16">Categorias</h3>
                        <form id="myForm">
                            @for ($i = 1; $i <= $payMonths ; $i++)
                                <label class="custom-control custom-radio">
                                    <input type="radio" name="radioName" value="{{$i}}" {{ $payMonths <= $i ? 'checked' : '' }}  class="custom-control-input">
                                        <span class="custom-control-label">Mes {{$i}}
                                    </span>
                                </label>

                            @endfor
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@include('postModal')
    <script>
        $(function(){
            $("body").on('click', '.postModal', function(){
                var id = $(this).data("id")
                $.get("/posts/"+id, function(r){
                    $("#postTitleModal").text(r.title)
                    if(r.type == 1){
                        $("#postContentModal").html(r.description)
                       
                        $("#postModal").modal()
                    }else{
                        $("#postContentModal").html(`
                            <video controls controlsList="nodownload" style="width:100%">
                                <source src="${r.video_path}" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
                            </video>`
                        )
                        $("#postModal").modal()
                       
                    }
                    
                    
                })
            })
            
            $('#myForm input').on('change', function() {
                var month = $('input[name=radioName]:checked', '#myForm').val();
                $.get('/getVideosMonth/'+month, function(r){
                    $('#videoRow').html(r)
                    $("#monthLabel").text(month)
                })
            });

        })
    </script>
@endsection
