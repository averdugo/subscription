<div class="modal fade" id="editModalV" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
	  	<div class="modal-content">
			<div class="modal-header">
		  		<h5 class="modal-title" id="exampleModalLabel">Editar Venta</h5>
		  			<button type="button" class="close  " data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
		 			 </button>
			</div>
			<form action="" method="POST">
                @csrf
                {{ method_field('put') }}

				<div class="modal-body">

					<div class="form-group row">
						<div class="form-group col-md-6">
							<label for="">Usuario</label>
							<input class="form-control form-control-lg" type="text" name="name" required=""  >
						</div>
						<div class="form-group col-md-6">
							<label for="">E-mail</label>
							<input class="form-control form-control-lg @error('email') is-invalid @enderror" type="email" name="email" required="" autocomplete="off" value="{{ old('email') }}">
							@error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
					</div>
					<div class="form-group row">
						<div class="form-group col-md-6">
							<label for="">Subcripcion</label>
							<input class="form-control form-control-lg" type="text" name="subscription_id" required="" >
                        </div>
                        <div class="form-group col-md-6">
							<label for="">Dias</label>
							<input class="form-control form-control-lg" type="text" name="days" required="" >
						</div>
					</div>
					<div class="form-group row">
						<div class="form-group col-md-6">
							<label for="">Total</label>
							<input class="form-control form-control-lg" type="text" name="amount" required="">
						</div>
						<div class="form-group col-md-6">
							<label for="">Estatus</label>
							<input class="form-control form-control-lg" type="text" name="status" required="">
						</div>
					</div>
					
					<div class="modal-footer">
						<div class="form-group pt-2 col-md-12 text-right">
							<button class="btn  btn-success" type="submit">Guardar</button>
						</div>
					</div>
				</div>		
			</form>									
	  	</div>
	</div>
</div>