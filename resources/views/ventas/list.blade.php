@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/buttons.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/select.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/fixedHeader.bootstrap4.css">
@endsection

@section('content')
    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

    </div>
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h3 class="mb-2">Ventas </h3>
            </div>
        </div>
    </div>
    <div class=row>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered first">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Usuario</th>
                                    <th>Rut</th>
                                    <th>Correo</th>
                                    <th>Subcripcion</th>
                                    <th>Dias</th>
                                    <th>Total</th>
                                    <th>Estado</th>
                                    <th style="width: 10%;"></th>   
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $i)
                                    <tr>
                                        <td>{{$i->created_at}}</td>
                                        <td>
                                            {{$i->user->f_name}} {{$i->user->l_name}}
                                        </td>
                                        <td>{{$i->user->rut}}</td>
                                        <td>{{$i->user->email}}</td>
                                        <td>{{$i->subscription->name}}</td>
                                        <td>{{$i->days}}</td>
                                        <td>{{$i->amount}}</td>
                                        <td>{{$i->status_label}}</td>
                                        <td>
                                            <button href="#" class="btn btn-default statusB" data-id="{{$i->id}}" >
                                                <i data-toggle="tooltip" data-placement="top" title="Editar" class="fa fa-pencil-alt" style="font-size: 18px;"></i>
                                               
                                            </a>

                                            <form action="/sales/{{ $i->id }}" method="post">
                                                @csrf
                                                {{ method_field('delete') }}
                                                <button class="btn btn-default" type="submit">
                                                    <i data-toggle="tooltip" data-placement="top" title="Borrar" class="fa fa-trash-alt" style="font-size: 18px;"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Fecha</th>
                                    <th >Usuario</th>
                                    <th >Correo</th>
                                    <th>Subcripcion</th>
                                    <th>Dias</th>
                                    <th>Total</th>
                                    <th>Estado</th>
                                    <th style="width: 10%;"></th>
                                   
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('ventas.edit')
@endsection

@section('scripts')
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="/assets/vendor/datatables/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="/assets/vendor/datatables/js/buttons.bootstrap4.min.js"></script>
    <script src="/assets/vendor/datatables/js/data-table.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
    @include('ventas.status')
    @include('ventas.edit')
    <script>
       
        $(function(){

            $("body").on('click','.goEdit', function(){
                var salesId = $(this).data('id');
                $.get('/sales/' + salesId, function( response ){
                    var sales = response;

                    $('#editModalV form').attr('action', "/sales/"+sales.id)
                    
                    $('#editModalV input[name=subscription_id]').val(sales.subscription_id)
                    $('#editModalV input[name=days]').val(sales.days)
                    $('#editModalV input[name=amount]').val(sales.amount)
                    $('#editModalV input[name=status]').val(sales.status)



                    $('#editModalV').modal()


                })

            })

            $("body").on('click','.goEdit', function(){
                var userId = $(this).data('id');
                $.get('/sales/' + userId, function( response ){
                    var user = response;
                    
                    $('#editModalV form').attr('action', "/sales/"+user.id)
                    
                    $('#editModalV input[name=name]').val(user.f_name)
                    $('#editModalV input[name=name]').val(user.l_name)
                    $('#editModalV input[name=email]').val(user.email)

                    $('#editModalV').modal()

                    
                })
                
            })

            $("body").on('click', '.cancelEdit', function(e){
                e.preventDefault()
                location.reload()
            })
            
            $("body").on('click', '.statusB', function(){
                var id = $(this).data("id")
                
                $.get('/sales/'+id, function(r){
                    console.log(r)
                    $('#statusModal form').attr('action', "/sales/"+r.id)
                   
                    $('#statusModal select[name=status]').val(r.status)
                    $('#statusModal').modal()
                })
            })
           
        })
    </script>
@endsection
