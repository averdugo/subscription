<div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Editar Estado</h5>
          <button type="button" class="close  cancelEdit" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="/sales" method="POST" enctype="multipart/form-data" >
                @csrf
                @method('put')
                <div class="form-group col-md-12">
                    <label for="">Estado</label>
                    <select name="status"  class="form-control form-control-lg" id="">
                        @foreach ($status as $k => $v)
                            <option value="{{$k}}">{{$v}}</option>
                        @endforeach
                    </select>
                </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info cancelEdit" >Cancelar</button>
          <button type="submit" class="btn btn-success">Guardar</button>
        </div>
            </form>
      </div>
    </div>
  </div>