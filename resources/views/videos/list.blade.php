@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/buttons.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/select.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/fixedHeader.bootstrap4.css">
@endsection

@section('content')
    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

    </div>
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h3 class="mb-2">Videos </h3>
            </div>
        </div>
    </div>
    <div class=row>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">
                    <button class="btn  float-right" data-toggle="modal" data-target="#addModal"style="
                    background-color: #8d68a9;
                    border-color: #8d68a9; color:white">Agregar</button>
                </h5>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered first">
                            <thead>
                                <tr>
                                    <th style="width: 10%;">Portada</th>
                                    <th style="width: 10%;">Video</th>
                                    <th>Titulo</th>
                                    <th>Descripcion</th>
                                    <th style="width: 10%;">Estado</th>
                                    <th style="width: 10%;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $i)
                                    <tr>
                                        <td><img src="{{ asset("storage/$i->img_path")}}" alt="" style="max-width: 200px;"></td>
                                        <td>
                                            <video controls controlsList="nodownload" style="max-width: 200px;">
                                                <source src="{{$i->video_path}}" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
                                            </video>
                                        </td>
                                        <td>{{$i->title}}</td>
                                        <td>{!! substr($i->description,0,100)!!}</td>
                                        <td>{{$i->status_label}}</td>
                                        <td>
                                        <button data-id="{{$i->id}}" class="editB btn btn-default float-left" style="width: 60px;"><i class="fas fa-edit"></i></button>
                                            <form action="{{ url('/posts', ['id' => $i->id]) }}" method="post" class="float-left">
                                                <button type="submit" class="btn btn-default" style="width: 60px"><i class="far fa-trash-alt"></i></button>
                                                @method('delete')
                                                @csrf
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th style="width: 10%;">Portada</th>
                                    <th style="width: 10%;">Video</th>
                                    <th>Titulo</th>
                                    <th>Descripcion</th>
                                    <th>Estado</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="/assets/vendor/datatables/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="/assets/vendor/datatables/js/buttons.bootstrap4.min.js"></script>
    <script src="/assets/vendor/datatables/js/data-table.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/17.0.0/classic/ckeditor.js"></script>
    @include('videos.add')
    @include('videos.edit')
    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );


        $(function(){
          
            $("body").on('click', '.editB', function(){
                var id = $(this).data("id")
                
                $.get('/posts/'+id, function(r){
                    console.log(r)
                    $('#editModal form').attr('action', "/posts/"+r.id)
                    $('#editModal input[name=title]').val(r.title)
                    $('#editModal input[name=video_path]').val(r.video_path)
                    $('#editModal input[name=month]').val(r.month)
                    $('#editModal textarea[name=description]').text(r.description)
                    $('#editModal select[name=status]').val(r.status)
                    ClassicEditor
                        .create( document.querySelector( '#editor2' ) )
                        .catch( error => {
                            console.error( error );
                        } );
                    $('#editModal').modal()
                })
            })
            $("body").on('click', '.cancelEdit', function(e){
                e.preventDefault()
                location.reload()
            })
        })
    </script>
@endsection
