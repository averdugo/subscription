<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel" >Agregar Video</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

            <form action="/posts" method="POST" enctype="multipart/form-data" >
                @csrf
                <input type="hidden" name="type" value="2">
                <div class="form-group col-md-12">
                    <label for="">Portada</label>
                    <input type="file" name="fileImg" class="form-control form-control-lg" placeholder="Selecciona Portada" >
                </div>
                <div class="form-group col-md-12">
                  <label for="">Video</label>
                  <input type="text" name="video_path" class="form-control form-control-lg" required=""  >
                </div>
                    
                  
                <div class="form-group col-md-12">
                    <label for="">Titulo</label>
                    <input class="form-control form-control-lg" type="text" name="title" required=""  >
                </div>
                
                <div class="form-group col-md-12">
                    <label for="">Descripcion</label>
                    <textarea  name="description" id="editor"></textarea>
                    <label for="">Este campo es obligatorio</label>
                </div>
                <div class="form-group col-md-12">
                  <label for="">Mes Correspondiente</label>
                  <input class="form-control form-control-lg" type="number" name="month" required=""  >
                </div>
                <div class="form-group col-md-12">
                    <label for="">Estado</label>
                    
                    <select name="status"  class="form-control form-control-lg" id="" required="">
                        <option value="">Selecciona</option>
                        <option value="1">Activo</option>
                        <option value="0">Inactivo</option>
                    </select>
                </div>
           

            

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-success"  style=" background-color: #8d68a9; border-color: #8d68a9; color:white">Guardar</button>
        </div>
    </form>
      </div>
    </div>
  </div>