<div class="modal fade" id="contactoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Contactanos</h5>
          <button type="button" class="close  cancelEdit" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
          <div class="">
              <ul class="list-unstyled mb-0">
              <li class="mb-2"><a href="mailto:servicio@mundosindicalapp.cl"><i class="fas fa-fw fa-envelope mr-2"></i>servicio@mundosindicalapp.cl</a></li>
              <li class="mb-0"><a ><i class="fas fa-fw fa-phone mr-2"></i>(+56) 9 9539 0998</a></li>
          </ul>
          </div>
        </div>
        
            
      </div>
    </div>
  </div>