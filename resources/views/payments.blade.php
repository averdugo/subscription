<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Mundo Sindical</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/libs/css/style.css">
    <link rel="stylesheet" href="/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
</head>

<body>
   
    <div class="dashboard-main-wrapper p-0">
      
        <nav class="navbar navbar-expand dashboard-top-header bg-white">
            <div class="container-fluid">
             
                <div class="dashboard-nav-brand">
                    <a class="dashboard-logo" href="/userlogout">MundoSindical</a>
                </div>
                
            </div>
        </nav>
        <div class="bg-light text-center">
            <div class="container">
                <div class="row">
                    <a class="dropdown-item btn btn-outline-danger col-md-1 mr-2" href="/userlogout" style="position: absolute;right: 0; display: block; height: 35px; width: 160px;"><i class="fas fa-power-off mr-2"></i>Cerrar Sesion</a>
                    <div class=" col-xl-12  col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="error-section">
                            <img src="/assets/images/logo.png" alt="" class="img-fluid" style="width: 280px">
                            <div class="error-section-content">
                                <h1 class="display-3">Suscripcion Suspendida</h1>
                                <p> Tienes que Renovar o Pagar tu suscripcion</p>
                                <div class="offset-xl-1 col-xl-10">
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                            <div class="section-block">
                                            <h3>Precios</h3>
                                        </div>
                                        </div>
                                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <div class="card">
                                                <div class="card-header bg-white text-center p-4 ">
                                                    <h3 class="mb-1">Mensual</h3>
                                                    <h1 class="mb-1"><span class="font-24">$</span><sub class="display-4">3.990</sub></h1>
                                                    <p>al mes</p>
                                                </div>
                                                <div class="card-body">
                                                   <ul class="list-unstyled bullet-check font-14  mb-0">
                                                        <li>4 Videos por Mes</li>
                                                        <li>Podrás Usar todos los Convenios de Mundo Sindical</li>
                                                        <li>Respuesta a tus Consultas Laborales</li>
                                                        <li>Acceso a la App Mundo Sindical</li>
                                                        <li>Formaras Parte de una Comunidad</li>
                                                    </ul>
                                                </div>
                                                <div class="card-body border-top">
                                                <a href="#" data-id="1" class="btn btn-outline-success btn-block btn-lg btnGoFlow">{{$user->subscription_id == 1 ? 'Renovar' : 'Suscribirse'}}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <div class="card">
                                                <div class="card-header bg-white text-center p-4 ">
                                                    <h3 class="mb-1">Semestral</h3>
                                                   
                                                     <h1 class=" mb-1"><span class="font-24">$</span><sub class="display-4">19.990</sub></h1>
                                                    <p>cada 6 meses</p>
                                                   
                                                </div>
                                                <div class="card-body">
                                                   <ul class="list-unstyled bullet-check mb-0">
                                                        <li>26 Videos por Mes</li>
                                                        <li>Podrás Usar todos los Convenios de Mundo Sindical</li>
                                                        <li>Respuesta a tus Consultas Laborales</li>
                                                        <li>Acceso a la App Mundo Sindical</li>
                                                        <li>Formaras Parte de una Comunidad</li>
                                                    </ul>
                                                </div>
                                                <div class="card-body border-top">
                                                    
                                                    <a href="#" data-id="2" class="btn btn-outline-success btn-block btn-lg btnGoFlow">{{$user->subscription_id == 2 ? 'Renovar' : 'Suscribirse'}}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <div class="card">
                                                <div class="card-header bg-white text-center p-4 ">
                                                    <h3 class="mb-1">Anual</h3>
                                                    <h1 class=" mb-1">
                                                        <span class="font-24">$</span><sub class="display-4"">34.990</sub>
                                                        
                                                    </h1>
                                                    <p style="margin: 0;">Pago Único</p>
                                                    <p style="margin: 0;">Siguientes 500</p>
                                                </div>
                                                <div class="card-body">
                                                   <ul class="list-unstyled bullet-check mb-0">
                                                        <li>52 Videos por Mes</li>
                                                        <li>Podrás Usar todos los Convenios de Mundo Sindical</li>
                                                        <li>Respuesta a tus Consultas Laborales</li>
                                                        <li>Acceso a la App Mundo Sindical</li>
                                                        <li>Formaras Parte de una Comunidad</li>
                                                        <li>Acceso Permanente a Ver los Videos</li>
                                                    </ul>
                                                </div>
                                                <div class="card-body border-top">
                                                    
                                                    <a href="#" data-id="3" class="btn btn-outline-success btn-block btn-lg btnGoFlow">{{$user->subscription_id == 3 ? '' : 'Suscribirse'}}</a>
                                                </div>

                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                   </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="bg-white p-3">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                            Copyright © 2020 Mundosindical. Todos los derechos reservados. Develop by <a href="https://www.ideasut.cl/">IdeaSut</a>.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    <script src="/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <script src="/assets/libs/js/main-js.js"></script>
    <script>
        $(function(){
            $("body").on('click', '.btnGoFlow', function(){
                var id = $(this).data('id')
                $.get("/changeSub/"+id, function(r){
                    if(r==1){
                        location.href = "/goFlow"
                    }else {
                        alert("Tenemos Problemas en este Momento favor esperar")
                    }
                    
                })
            })
        })
    </script>
</body>
 
</html>