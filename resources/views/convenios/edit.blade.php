<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Editar Convenio</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="/posts" method="POST" enctype="multipart/form-data" >
                @csrf
                @method('put')
                <input type="hidden" name="type" value="1">
                <div class="form-group col-md-12">
                    <label for="">Portada</label>
                    <input type="file" name="fileImg" class="form-control form-control-lg" placeholder="Selecciona Portada" >
                </div>
                <div class="form-group col-md-12">
                    <label for="">Titulo</label>
                    <input class="form-control form-control-lg" type="text" name="title" required=""  >
                </div>
                <div class="form-group col-md-12">
                    <label for="">Descripcion</label>
                    <textarea  name="description" id="editor2"></textarea>
                </div>
                <div class="form-group col-md-12">
                    <label for="">Estado</label>
                    <select name="status"  class="form-control form-control-lg" id="">
                        <option value="">Selecciona</option>
                        <option value="1">Activo</option>
                        <option value="0">Inactivo</option>
                    </select>
                </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info cancelEdit" >Cancelar</button>
          <button type="submit" class="btn btn-success">Guardar</button>
        </div>
            </form>
      </div>
    </div>
  </div>