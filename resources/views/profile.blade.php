@extends('layouts.app')

@section('content')
    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h3 class="mb-2">Perfil </h3>
                
            </div>
        </div>
    </div>
    <div class="row">
        <!-- ============================================================== -->
        <!-- profile -->
        <!-- ============================================================== -->
        <div class="col-xl-3 col-lg-3 col-md-5 col-sm-12 col-12">
            <!-- ============================================================== -->
            <!-- card profile -->
            <!-- ============================================================== -->
            <div class="card">
                <div class="card-body">
                    <a data-id="{{$user->id}}" class="btn btn-ligth btn-xs editUser" style="float: right;margin-right: -20px;margin-left: -20px;z-index: 999;"><i class="fas fa-edit"></i></a>
                    <div class="user-avatar text-center d-block">
                        @if ($user->img)
                            <img src="{{ asset("storage/$user->img")}}" alt="User Avatar" class="rounded-circle user-avatar-xxl">
                        @else
                            <img src="/assets/images/avatar-1.jpg" alt="User Avatar" class="rounded-circle user-avatar-xxl">
                              
                        @endif
                        
                    </div>
                    <div class="text-center">
                        <h2 class="font-24 mb-0">{{$user->full_name}}</h2>
                        <p>Rut: {{$user->rut}}</p>
                    </div>
                </div>
                <div class="card-body border-top">
                    <h3 class="font-16">Informacion</h3>
                    <div class="">
                        <ul class="list-unstyled mb-0">
                        <li class="mb-2"><i class="fas fa-fw fa-envelope mr-2"></i>{{$user->email}}</li>
                        <li class="mb-0"><i class="fas fa-fw fa-phone mr-2"></i>{{$user->phone}}</li>
                        <li class="mb-0"><i class="fas fa-fw fa-address-book mr-2"></i>{{$user->address}}</li>
                        <li class="mb-0"><i class="fas fa-fw fa-address-book mr-2"></i>{{$user->city}}</li>
                        <li class="mb-0"><i class="fas fa-fw  fa-birthday-cake mr-2"></i>{{ date('d-M-Y', strtotime($user->birthday_at)) }}</li>
                        
                    </ul>
                    </div>
                </div>
               
                
                
            </div>
            <!-- ============================================================== -->
            <!-- end card profile -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end profile -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- campaign data -->
        <!-- ============================================================== -->
        <div class="col-xl-9 col-lg-9 col-md-7 col-sm-12 col-12">
            <!-- ============================================================== -->
            <!-- campaign tab one -->
            <!-- ============================================================== -->
            <div class="influence-profile-content pills-regular">
                <ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-campaign-tab" data-toggle="pill" href="#pills-campaign" role="tab" aria-controls="pills-campaign" aria-selected="true">Estadisticas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-packages-tab" data-toggle="pill" href="#pills-packages" role="tab" aria-controls="pills-packages" aria-selected="false">Suscripciones</a>
                    </li>
                    <li class="nav-item sr-only">
                        <a class="nav-link" id="pills-review-tab" data-toggle="pill" href="#pills-review" role="tab" aria-controls="pills-review" aria-selected="false">Reviews</a>
                    </li>
                  
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-campaign" role="tabpanel" aria-labelledby="pills-campaign-tab">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="section-block">
                                    <h3 class="section-title">Mis Estadisticas</h3>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h1 class="mb-1">{{$user->monthviews}}</h1>
                                        <p>Vistos Mensual</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h1 class="mb-1">{{$user->monthviews}}</h1>
                                        <p>Vistos Anual</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h1 class="mb-1">{{$user->missingvideos}}</h1>
                                        <p>Videos Faltantes</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h1 class="mb-1">{{$user->missingdays}}</h1>
                                        <p>Dias Faltantes</p>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="section-block">
                            <h3 class="section-title">Vistos</h3>
                        </div>
                        @foreach ($user->views as $i)
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col-xl-9 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="user-avatar float-xl-left pr-4 float-none">
                                            @php
                                                $img = $i->post->img_path;
                                            @endphp
                                            <img src="{{ asset("storage/$img")}}" alt="User Avatar" class="user-avatar-xl">    
                                        </div>
                                        <div class="pl-xl-3">
                                            <div class="m-b-0">
                                                <div class="user-avatar-name d-inline-block">
                                                    <h2 class="font-24 m-b-10">{{$i->post->title}}</h2>
                                                </div>
                                            </div>
                                            <div class="user-avatar-address">
                                                <p class="mb-2">{!!$i->post->description!!}</p>
                                                        
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-lg-12 col-md-12 col-sm-12 col-12 sr-only" >
                                        <div class="float-xl-right float-none mt-xl-0 mt-4">
                                            @if ($i->type==1)
                                                <a href="#" data-id="{{$i->post->id}}" class="btn btn-primary postModal" >Leer Mas</a>    
                                            @else
                                                <a href="#" data-id="{{$i->post->id}}" class="btn btn-primary postModal" >Ver Video</a>
                                            @endif
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>   
                        @endforeach
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col-xl-9 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="user-avatar float-xl-left pr-4 float-none">
                                            <img src="storage/$i->img_path" alt="User Avatar" class="user-avatar-xl">    
                                        </div>
                                        <div class="pl-xl-3">
                                            <div class="m-b-0">
                                                <div class="user-avatar-name d-inline-block">
                                                    <h2 class="font-24 m-b-10">titulo</h2>
                                                </div>
                                            </div>
                                            <div class="user-avatar-address">
                                                <p class="mb-2">description</p>
                                                        
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="float-xl-right float-none mt-xl-0 mt-4">
                                            
                                                <a href="#" data-id="20" class="btn  postModal" style="
                                                background-color: #8d68a9;
                                                border-color: #8d68a9; color:white">Ver Video</a>
                                          
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        
                        
                    </div>
                    <div class="tab-pane fade" id="pills-packages" role="tabpanel" aria-labelledby="pills-packages-tab">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="section-block">
                                    <h2 class="section-title">Suscripciones</h2>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <div class="card">
                                    <div class="card-header bg-white text-center p-4 ">
                                        <h3 class="mb-1">Mensual</h3>
                                        <h1 class="mb-1"><span class="font-24">$</span><sub class="display-4">3.990</sub></h1>
                                        <p>al mes</p>
                                    </div>
                                    <div class="card-body">
                                       <ul class="list-unstyled bullet-check font-14  mb-0">
                                            <li>4 Videos por Mes</li>
                                            <li>Podrás Usar todos los Convenios de Mundo Sindical</li>
                                            <li>Respuesta a tus Consultas Laborales</li>
                                            <li>Acceso a la App Mundo Sindical</li>
                                            <li>Formaras Parte de una Comunidad</li>
                                        </ul>
                                    </div>
                                    <div class="card-body border-top">
                                        <a href="#" data-id="1" class="btn btn-outline-success btn-block btn-lg btnGoFlow">{{$user->subscription_id == 1 ? 'Renovar' : 'Suscribirse'}}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <div class="card">
                                    <div class="card-header bg-white text-center p-4 ">
                                        <h3 class="mb-1">Semestral</h3>
                                       
                                         <h1 class=" mb-1"><span class="font-24">$</span><sub class="display-4">19.990</sub></h1>
                                        <p>cada 6 meses</p>
                                       
                                    </div>
                                    <div class="card-body">
                                       <ul class="list-unstyled bullet-check mb-0">
                                            <li>26 Videos por Mes</li>
                                            <li>Podrás Usar todos los Convenios de Mundo Sindical</li>
                                            <li>Respuesta a tus Consultas Laborales</li>
                                            <li>Acceso a la App Mundo Sindical</li>
                                            <li>Formaras Parte de una Comunidad</li>
                                        </ul>
                                    </div>
                                    <div class="card-body border-top">
                                        
                                        <a href="#" data-id="2" class="btn btn-outline-success btn-block btn-lg btnGoFlow">{{$user->subscription_id == 2 ? 'Renovar' : 'Suscribirse'}}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <div class="card">
                                    <div class="card-header bg-white text-center p-4 ">
                                        <h3 class="mb-1">Anual</h3>
                                        <h1 class=" mb-1">
                                            <span class="font-24">$</span><sub class="display-4">34.990</sub>
                                        </h1>
                                    </div>
                                    <div class="card-body">
                                       <ul class="list-unstyled bullet-check mb-0">
                                            <li>52 Videos por Mes</li>
                                            <li>Podrás Usar todos los Convenios de Mundo Sindical</li>
                                            <li>Respuesta a tus Consultas Laborales</li>
                                            <li>Acceso a la App Mundo Sindical</li>
                                            <li>Formaras Parte de una Comunidad</li>
                                            <li>Acceso Permanente a Ver los Videos</li>
                                        </ul>
                                    </div>
                                    <div class="card-body border-top">
                                        <a href="#" data-id="3" class="btn btn-outline-success btn-block btn-lg btnGoFlow">{{$user->subscription_id == 3 ? '' : 'Suscribirse'}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                   
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end campaign tab one -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end campaign data -->
        <!-- ============================================================== -->
    </div>
   
@endsection

@section('scripts')
    @include('usuarios.edit')
    <script>
        $(function(){
            $("body").on('click', '.btnGoFlow', function(){
                var id = $(this).data('id')
                $.get("/changeSub/"+id, function(r){
                    if(r==1){
                        location.href = "/goFlow"
                    }else {
                        alert("Tenemos Problemas en este Momento favor esperar")
                    }
                    
                })
            })
            
            $("body").on('click', '.editUser', function(){
                var id = $(this).data("id");
                $.get("/users/"+id, function(r){
                    console.log(r)
                    $('#editModal form').attr('action', "/users/"+r.id)
                    $('#editModal input[name=f_name]').val(r.f_name)
                    $('#editModal input[name=l_name]').val(r.l_name)
                    $('#editModal input[name=rut]').val(r.rut)
                    $('#editModal input[name=phone]').val(r.phone)
                    $('#editModal input[name=address]').val(r.address)
                    $('#editModal input[name=city]').val(r.city)
                    $('#editModal input[name=birthday_at]').val(r.birthday_at)
                    $('#editModal select[name=gender]').val(r.gender)
                    $('#editModal input[name=email]').val(r.email)

                    $('#editModal').modal()
                })
            })
        })
    </script>
@endsection