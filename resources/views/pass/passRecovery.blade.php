<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <img src="/assets/images/logo.png" alt="" style="max-width: 180px;height: 191px;margin-top: 14px;">
        <h3 style="font-size: 30px; font-family: 'Circular Std Medium';">Asunto: recuperacion de password</h3>
        <p style="font-size: 30px; font-family: 'Circular Std Medium';">
            <div  style="font-size: 30px; font-family: 'Circular Std Medium'; margin:6px;">
                ¡Hola! Que gusto saludarte 
            </div>
            <div style="font-size: 30px; font-family: 'Circular Std Medium'; margin:6px; margin-top: 60px;">
                Esperamos que te encuentres muy bien junto a tu familia.
                Te escribimos para que recuperes tu password
                No te quedes fuera de la comunidad de trabajadores informados y sigue educándote en
                Legislación Laboral con Mundo Sindical.
                Visita la página web <a href="www.mundosindicalapp.com">www.mundosindicalapp.com</a>.
            </div>
            <div style="font-size: 30px; font-family: 'Circular Std Medium'; margin:6px; margin-top: 60px;">
                Recuerda que todo trabajador en Chile debe conocer sus derechos laborales para que nuestros
                derechos no sean vulnerados y que un Trabajador Informado es un Trabajador Completo.
            <div style="font-size: 30px; font-family: 'Circular Std Medium; margin:6px; margin-top: 60px;">
                ¡Te esperamos!
                </div>            
            <div style="font-size: 30px; font-family: 'Circular Std Medium'; margin:6px; margin-top: 60px;">

                Éxito.
            </div>
            <div style="font-size: 30px; font-family: 'Circular Std Medium'; margin:6px; margin-top: 60px;">

                Firma:
            </div>
        </p>
        <div>
            <img src="{{$message->embed(public_path('assets/images/footer.png'))}}" / width="100%">
        </div>
</body>
</html>