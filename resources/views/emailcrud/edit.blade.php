@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/buttons.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/select.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/fixedHeader.bootstrap4.css">
@endsection

@section('content')
    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

    </div>
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h3 class="mb-2">Emails </h3>
            </div>
        </div>
    </div>
    <div class=row>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-body">
                    <form action="/emails/{{$email->id}}" method="POST" enctype="multipart/form-data" >
                        @csrf
                        @method('put')
                        <input type="hidden" name="type" value="{{$email->type}}">
                        
                       
                        <div class="form-group col-md-12">
                            <label for="">Asunto</label>
                            <input class="form-control form-control-lg" type="text" name="subject" value="{{$email->subject}}" >
                        </div>
                        <div class="form-group col-md-12">
                            <label for="">Cuerpo</label>
                            <textarea  name="body" id="editor">{{$email->body}}</textarea>
                        </div>
                       <div>
                        <button class="btn  float-right" style="background-color: #8d68a9;border-color: #8d68a9; color:white">Actualizar</button>
                       </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.ckeditor.com/ckeditor5/17.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>
@endsection