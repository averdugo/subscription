<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <img src="{{$message->embed(public_path('/assets/images/logo.png'))}}" alt="" style="max-width: 180px;height: 191px;margin-top: 14px;">
    <div  style="font-size: 22px; font-family: 'Circular Std Medium'; margin:6px;">
        Cuentas Por Suspender 
    </div>
    @foreach ($data['users'] as $i)
        <table class="table table-striped table-bordered first">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Rut</th>
                    <th>Email</th>
                    <th>Telefono</th>
                    <th>tipo de usuario</th>
                    <th>Suscripcion</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $i)
                    <tr>
                        <td>{{$i->full_name}}</td>
                        <td>{{$i->rut}}</td>
                        <td>{{$i->email}}</td>
                        <td>{{$i->phone}}</td>
                        <td>{{$i->type}}</td>
                        <td>{{$i->subscription ? $i->subscription->name:''}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endforeach
<img src="{{$message->embed(public_path('assets/images/footer.png'))}}" / width="100%">
</body>
</html>
