<div class="modal fade" id="crearModalS" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
	  	<div class="modal-content">
			<div class="modal-header">
		  		<h5 class="modal-title" id="exampleModalLabel">Crear Suscripcion</h5>
		  			<button type="button" class="close  " data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
		 			 </button>
			</div>
			<form action="/subscription" method="POST">
                @csrf
			
				<div class="modal-body">
				 	<div class="form-group row">
						<div class="form-group col-md-6">
							<label for="">Nombre</label>
							<input class="form-control form-control-lg" type="text" name="name" value=""  required=""  >
						</div>
						<div class="form-group col-md-6">
							<label for="">Descripcion</label>
							<input class="form-control form-control-lg" type="text" name="description" required="" >
						</div>
					</div>
					<div class="form-group row">
                        <div class="form-group col-md-6">
							<label for="">Price</label>
							<input class="form-control form-control-lg" type="text" name="price" required="" >
						</div>
                        <div class="form-group col-md-6">
                            <label for="">Suscripcion</label>
                            <select class="form-control form-control-lg" name="subscription_id"  >
                                <option value="">Seleccione</option>
                                @foreach ($sus as $s)
                                    <option value="{{$s->id}}">{{$s->name}}</option>
                                @endforeach
                            </select>
                        </div>
					</div>
			   
					<div class="modal-footer">
						<div class="form-group pt-2 col-md-12 text-right">
							<button class="btn  btn-success" type="submit">Guardar</button>
						</div>
					</div>
				</div>		
			</form>								
	  	</div>
	</div>
</div>