<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <img src="{{$message->embed(public_path('/assets/images/logo.png'))}}" alt="" style="max-width: 180px;height: 191px;margin-top: 14px;">
    <p style="font-size: 22px; font-family: 'Circular Std Medium';">
        <div  style="font-size: 22px; font-family: 'Circular Std Medium'; margin:6px;">
            ¡Hola! Que gusto saludarte {!!$data['user']->f_name!!}
        </div>
        {!!$data['emailData']->body!!}
    </p>
    <img src="{{$message->embed(public_path('assets/images/footer.png'))}}" / width="100%">
</body>
</html>
