@extends('layouts.app')

@section('css')
	<link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/dataTables.bootstrap4.css">
	<link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/buttons.bootstrap4.css">
	<link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/select.bootstrap4.css">
	<link rel="stylesheet" type="text/css" href="../assets/vendor/datatables/css/fixedHeader.bootstrap4.css">
@endsection

@section('content')
	<div class="card-body">
		@if (session('status'))
			<div class="alert alert-success" role="alert">
				{{ session('status') }}
			</div>
		@endif

	</div>
	<div class="row">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
			<div class="page-header row">
				<div class="col-md-5 align-self-center">
					<h2 class="mb-2">Suscripciones </h2>
				</div>
				<div class="col-md-7 text-right">
                    <a href="#" data-toggle="modal" data-target="#crearModalS" class="btn waves-effect waves-light btn btn-info pull-right hidden-sm-down" style="
					background-color: #8d68a9;
					border-color: #8d68a9; color:white">Crear Suscripcion</a>
                </div>
                @include('suscripciones.crear')
			</div>

		</div>
	<div class="row" style="width:100%">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
			<div class="card">
				
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered first">
							<thead>
								<tr>
									<th>Nombre</th>
									<th>Descripcion</th>
									<th>Precio</th>
									<th style="width: 10%;"></th>
									
								</tr>
							</thead>
							<tbody>
									@foreach ($data as $i)
									<tr>
										<td>{{$i->name}}</td>
										<td>{{$i->description}}</td>
										<td>{{$i->price}}</td>
										<td>
                                            <button href="#" class="btn btn-default link m-r-10 goEdit" data-id="{{$i->id}}" >
                                                <i data-toggle="tooltip" data-placement="top" title="Editar" class="fa fa-pencil-alt" style="font-size: 18px;"></i>
                                               
                                            </button>

                                            <form action="/subscription/{{ $i->id }}" method="post">
                                                @csrf
                                                {{ method_field('delete') }}
                                                <button class="btn btn-default " type="submit">
                                                    <i data-toggle="tooltip" data-placement="top" title="Borrar" class="fa fa-trash-alt" style="font-size: 18px;"></i>
                                                </button>
                                            </form>
                                            
                                        </td>
									</tr>
									@endforeach
						
							</tbody>
							<tfoot>
								<tr>
										<th>Nombre</th>
										<th>Descripcion</th>
										<th>Precio</th>
										<th style="width: 10%;"></th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('suscripciones.edit')
@endsection

@section('scripts')
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="/assets/vendor/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
	<script src="/assets/vendor/datatables/js/buttons.bootstrap4.min.js"></script>
	<script src="/assets/vendor/datatables/js/data-table.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
	<script src="https://cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js"></script>
	<script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
	<script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
	<script src="https://cdn.ckeditor.com/ckeditor5/17.0.0/classic/ckeditor.js"></script>

	<script>
		$(function(){


			$("body").on('click','.goEdit', function(){
				var subscriptionId = $(this).data('id');
				$.get('/subscription/' + subscriptionId, function( response ){
					var subscription = response;
					
					$('#editModalS form').attr('action', "/subscription/"+subscription.id)
					
					$('#editModalS input[name=name]').val(subscription.name)
					$('#editModalS input[name=description]').val(subscription.description)
					$('#editModalS input[name=price]').val(subscription.price)



					$('#editModalS').modal()

		
				})
	
			})

			$("body").on('click', '.cancelEdit', function(e){
				e.preventDefault()
				location.reload()
				})
		})
	</script>
@endsection
