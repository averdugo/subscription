@extends('layouts.auth')

@section('content')
<form class="splash-container" method="POST" action="{{ route('register') }}" style="max-width:700px">
    @csrf
    <input type="hidden" name="type" value="2">
    <input type="hidden" name="status" value="1">
    <div class="card">
       
        <div class="card-header text-center">
            <a href="/">
                <img class="logo-img" src="/assets/images/logo.png" alt="logo" style="width: 250px">
            </a>
            <h3 class="mb-1">Formulario de Registro</h3>
            <span class="splash-description">Porfavor Ingresa Tus Datos.</span>
        </div>
        <div class="card-body row">
            
            <div class="form-group col-md-6">
                <label for="">Nombre</label>
                <input class="form-control form-control-lg @error('f_name') is-invalid @enderror" type="text" name="f_name" >
                @error('f_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>Este campo es obligatorio</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group col-md-6">
                <label for="">Apellido</label>
                <input class="form-control form-control-lg @error('l_name') is-invalid @enderror" type="text" name="l_name" >
                @error('l_name')
                <span class="invalid-feedback" role="alert">
                    <strong>Este campo es obligatorio</strong>
                </span>
            @enderror
            </div>
            <div class="form-group col-md-6">
                <label for="">Rut</label>
                <input class="form-control form-control-lg @error('rut') is-invalid @enderror" type="text" name="rut"  autocomplete="off" placeholder="12345465-9">
                @error('rut')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
            <div class="form-group col-md-6">
                <label for="">Edad</label>
                <input class="form-control form-control-lg  type="text" name="age" >
            </div>
            <div class="form-group col-md-6">
                <label for="">Telefono</label>
                <input class="form-control form-control-lg @error('phone') is-invalid @enderror" type="text" name="phone"  placeholder="+569-12345678">
                @error('phone')
                <span class="invalid-feedback" role="alert">
                    <strong>Este campo es obligatorio</strong>
                </span>
            @enderror
            </div>
            <div class="form-group col-md-6">
                <label for="">E-mail</label>
                <input class="form-control form-control-lg @error('email') is-invalid @enderror" type="email" name="email" autocomplete="off" value="{{ old('email') }}">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group col-md-6">
                <label for="">Password</label>
                <input name="password" class="form-control form-control-lg @error('password') is-invalid @enderror" id="pass1" type="password" >
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group col-md-6">
                <label for="">Confirmar Password</label>
                <input type="password" name="password_confirmation"  class="form-control form-control-lg"  placeholder="Confirm">
            </div>
            <div class="form-group col-md-12">
                <label for="">Tipo de Subscripcion</label>
                <select id="subscriptionselect" class="form-control form-control-lg" name="subscription_id" placeholder="Genero">
                    
                </select>
            </div>
            <div class="form-group pt-2 col-md-12 text-right">
                <button class="btn  btn-success" type="submit">Registrar y Pagar</button>
            </div>
            <div class="form-group sr-only">
                <label class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox"><span class="custom-control-label">By creating an account, you agree the <a href="#">terms and conditions</a></span>
                </label>
            </div>
            
        </div>
        <div class="card-footer bg-white">
            <p>Ya tienes Cuenta? <a href="/login" class="text-success">Logueate.</a></p>
        </div>
    </div>
</form>

@endsection

@section('scripts')
    <script>
        $(function(){
            $.get('/subscriptionselect',function(r){
                $("#subscriptionselect").html(r)
            })
        })
    </script>
@endsection